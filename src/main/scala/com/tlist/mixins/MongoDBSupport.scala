package com.tlist.app.mixins

import com.mongodb.casbah.Imports._
import org.scalatra._

trait MongoDBSupport {
	
	val dbname = "bartr"

	val mongoClient =  MongoClient()
    val mongodb = mongoClient( dbname  )

    val mongo_users = mongodb("users")
    val mongo_movies = mongodb("movies")

}
