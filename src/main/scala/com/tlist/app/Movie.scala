package com.tlist.app

import com.tlist.app.mixins._
import com.mongodb.casbah.Imports._



object MovieFactory extends MongoDBSupport {
   
   def getMovieBySlug( slug : String ) : Option[Movie] = {
      
      mongo_movies.find( MongoDBObject( "slug" -> slug ) ) 

      val head = mongo_movies.headOption
      head match {
         case None => None
         case Some(movie) => Some(Movie( movie("name").toString, movie("slug").toString, movie.as[Boolean] ( "onNetflix")  ))
      
      }

   }


}

case class Movie( name: String , slug : String, onNetflix : Boolean )
