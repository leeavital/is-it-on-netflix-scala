package com.tlist.app;

import com.tlist.app.mixins._
import com.mongodb.casbah.Imports._




abstract class Task{}

case class ToDoTask(contents : String ) extends Task 

case class DoneTask( contents : String ) extends Task 



object TaskFactory extends MongoDBSupport {
	
	def getTasksFromUser( u : User ) : List[User]  = {

		// unpack user
		val User(name, _) = u

		val query = MongoDBObject( "name" -> name )

		mongo_users.find( query )

		val tasks = mongo_users.head.get( "tasks" )


		List()

	}

}
