package com.tlist.app;

import com.mongodb.casbah.Imports._
import com.tlist.app.mixins._

case class User(name: String, pass: String) {
	
	// no op

}

													// wat?
object Users extends MongoDBSupport {

	def getFromNameAndPassword( name: String, pass: String )  : Option[User] = {
		
		
		mongo_users.find( MongoDBObject("name" -> name, "hash" -> pass ) );

		val head = mongo_users.headOption

		head match {
			case None => None

			case Some(_) => Some( User(name, pass) )
		}

	}


	def registerUserWithNameAndPass( name : String, pass : String ) : User = {

		val user : MongoDBObject = MongoDBObject( "_id" -> name, "name" -> name, "hash" -> pass, "tasks" -> List())

		mongo_users += user

		User( name, pass )

	}
}

// Executors of the great fire of justice