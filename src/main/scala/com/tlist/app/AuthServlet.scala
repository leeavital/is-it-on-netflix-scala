package com.tlist.app

import org.scalatra._
import scalate.ScalateSupport
import com.tlist.app.mixins._
import com.mongodb.casbah.Imports._


import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json._



class AuthServlet extends TListServletStack    {


	

	val UserFactory = Users



	// Before every action runs, set the content type to be in JSON format. JacksonJsonSupport will rewrite requests 
	// to use JSON serialization
  	before() {
    	contentType = formats("json")
  	}



	post("/newuser"){
		
		val uname : Option[String] = params.get( "uname")
		val pass : Option[String] = params.get( "pass" )

		(uname, pass) match {
			case (Some(name), Some(pass)) => UserFactory.registerUserWithNameAndPass( name, pass )
			case _ => 
				Map( "error" -> "Missing args" )



		}

	}


	post("/log_in"){

		val uname : String = params( "uname")
		val pass : String = params( "pass" )



		val user : Option[User] = UserFactory.getFromNameAndPassword( uname, pass )

		user match {
			case Some(u) => 
            session("uname") = uname ; 
            session("logged_in") = true; 
            ; u
			case None => false 
		}

	}

   get("/current_session") {

      session("uname")

   }


}
