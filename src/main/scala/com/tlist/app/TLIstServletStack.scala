package com.tlist.app

import org.scalatra._
import scalate.ScalateSupport
import com.tlist.app.mixins._
import com.mongodb.casbah.Imports._


import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json._


class TListServletStack extends ScalatraServlet with JacksonJsonSupport{
	
	protected implicit val jsonFormats: Formats = DefaultFormats

}
